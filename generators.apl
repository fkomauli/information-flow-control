func split (number) = (number, number).
split(o) = (o, o).
split(s(N)) = (o, s(N)).
split(s(N)) = (s(N'), N'') :- (N', N'') = split(N).

func gen_value (number, program) = program.
gen_value(s(o), P) = [push((private, o)) | P].
gen_value(s(o), P) = [push((public, o)) | P].
gen_value(s(s(N)), P) = gen_value(s(N), [load | P]).
gen_value(s(s(s(N))), P) = R :-
    (s(N'), s(N'')) = split(s(s(N))),
    Q = gen_value(s(N'), [add | P]),
    R = gen_value(s(N''), Q).

func append (program, program) = program.
append([], P) = P.
append([I | P], P') = [I | append(P, P')].

func shuffle_append2 (program, program, program) = program.
shuffle_append2(P', P'', P) = append(P', append(P'', P)).
shuffle_append2(P', P'', P) = append(P'', append(P', P)).

func shuffle_append3 (program, program, program, program) = program.
shuffle_append3(P', P'', P''', P) = append(P', append(P'', append(P''', P))).
shuffle_append3(P', P'', P''', P) = append(P'', append(P', append(P''', P))).
shuffle_append3(P', P'', P''', P) = append(P'', append(P''', append(P', P))).

func gen_sequence (number, program) = program.
gen_sequence(s(s(L)), P) = gen_value(s(L), [jump | P]).
gen_sequence(s(s(L)), P) = Q :-
    (s(L'), L'') = split(s(L)),
    S = gen_sequence(L'', []),
    V = gen_value(s(L'), []),
    Q = shuffle_append2(S, V, [jump | P]).
gen_sequence(s(s(s(L))), P) = R :-
    (s(N), s(N')) = split(s(s(L))),
    Q = gen_value(s(N'), [store | P]),
    R = gen_value(s(N), Q).
gen_sequence(s(s(s(L))), P) = Q :-
    (s(s(L')), L'') = split(s(s(L))),
    S = gen_sequence(L'', []),
    (s(N), s(N')) = split(s(s(L'))),
    V = gen_value(s(N), []),
    V' = gen_value(s(N'), []),
    Q = shuffle_append3(S, V, V', [store | P]).

func gen_program (number) = program.
gen_program(o) = [halt].
gen_program(s(L)) = gen_sequence(s(L), [halt]).

func gen_number (number) = number.
gen_number(s(N)) = gen_number(N).
gen_number(N) = N.

func gen_programs (number, program) = (program, program).
gen_programs(_, [halt]) = ([halt], [halt]).
gen_programs(L, [I | P]) = ([I | P'], [I | P'']) :- (P', P'') = gen_programs(L, P).
gen_programs(L, [push((private, o)) | P]) = ([push((private, N')) | P'], [push((private, N'')) | P'']) :-
    (P', P'') = gen_programs(L, P),
    N' = gen_number(L),
    N'' = gen_number(L).
gen_programs(s(L), [push((public, o)) | P]) = ([push((public, s(N))) | P'], [push((public, s(N))) | P'']) :-
    (P', P'') = gen_programs(s(L), P),
    N = gen_number(L).

func gen_programs_upto (number) = (program, program).
gen_programs_upto(s(L)) = gen_programs_upto(L).
gen_programs_upto(L) = (P', P'') :- P = gen_program(L), (P', P'') = gen_programs(L, P).

func gen_memory (number) = memory.
gen_memory(o) = [].
gen_memory(s(N)) = [(public, o) | gen_memory(N)].
