%% NATURAL NUMBERS %%

number: type.
o: number.
s: number -> number.

func sum (number, number) = number.
sum(o, N) = N.
sum(s(N), N') = sum(N, s(N')).


%% LABELS %%

label: type.
public: label.
private: label.

pred subsecret (label, label).
subsecret(private, private).
subsecret(public, _).

func join (label, label) = label.
join(public, L) = L.
join(private, _) = private.


%% MACHINE STATES %%

type value = (label, number).

instruction: type.
push: value -> instruction.
pop: instruction.
load: instruction.
store: instruction.
add: instruction.
noop: instruction.
jump: instruction.
halt: instruction.

type pc = value.
type stack = [value].
type memory = [value].
type program = [instruction].

type state = (pc, stack, memory, program).

func fetch (memory, number) = value.
fetch([V | _], o) = V.
fetch([_ | M], s(I)) = fetch(M, I).

func save (memory, number, value) = memory.
save([_ | M], o, V) = [V | M].
save([V' | M], s(I), V) = [V' | save(M, I, V)].

func read (number, program) = instruction.
read(o, [I | _]) = I.
read(s(PC), [_ | P]) = read(PC, P).


%% INDISTINGUISHABILITY %%

pred indistinguishable_values (value, value).
indistinguishable_values((private, _), (private, _)).
indistinguishable_values((public, V), (public, V)).

pred indistinguishable_instructions (instruction, instruction).
indistinguishable_instructions(I, I).
indistinguishable_instructions(push(V), push(V')) :- indistinguishable_values(V, V').

pred indistinguishable_memories ([value], [value]).
indistinguishable_memories([], []).
indistinguishable_memories([V | M], [V' | M']) :- indistinguishable_values(V, V'), indistinguishable_memories(M, M').

pred indistinguishable_programs (program, program).
indistinguishable_programs([], []).
indistinguishable_programs([I | P], [I' | P']) :- indistinguishable_instructions(I, I'), indistinguishable_programs(P, P').

pred indistinguishable_states (state, state).
indistinguishable_states(((private, _), _, _, _), ((private, _), _, _, _)).
indistinguishable_states(((public, _), _, M, P), ((public, _), _, M', P')) :- indistinguishable_memories(M, M'), indistinguishable_programs(P, P').


%% DYNAMIC SEMANTICS %%

func initial_state (program) = state.
initial_state(P) = ((public, o), [], [], P).
initial_state(P) = (PC, S, [(public, o) | M], P) :- (PC, S, M, P) = initial_state(P).

func step (state) = state.
step(((LPC, PC), S, M, P)) = ((LPC, s(PC)), S, M, P) :- read(PC, P) = noop.
step(((LPC, PC), S, M, P)) = ((LPC, s(PC)), [V | S], M, P) :- read(PC, P) = push(V).
step(((LPC, PC), [_ | S], M, P)) = ((LPC, s(PC)), S, M, P) :- read(PC, P) = pop.
step(((LPC, PC), [(L, I) | S], M, P)) = ((LPC, s(PC)), [(join(L, L'), N) | S], M, P) :- read(PC, P) = load, (L', N) = fetch(M, I).
step(((LPC, PC), [(L, I), (L', N) | S], M, P)) = ((LPC, s(PC)), S, M', P) :- read(PC, P) = store, (L'', _) = fetch(M, I), subsecret(L, L''), M' = save(M, I, (join(L, L'), N)).
step(((LPC, PC), [(L, N), (L', N') | S], M, P)) = ((LPC, s(PC)), [(join(L, L'), sum(N, N')) | S], M, P) :- read(PC, P) = add.
step(((_, PC), [(L, N) | S], M, P)) = ((L, N), S, M, P) :- read(PC, P) = jump.

func steps (state) = state.
steps(S) = S.
steps(S) = steps(step(S)).

func run (state) = state.
run(X) = ((L, PC), S, M, P) :- ((L, PC), S, M, P) = steps(X), read(PC, P) = halt.
