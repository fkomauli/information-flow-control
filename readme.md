### Information-Flow Control (IFC)

Abstract machine with information-flow semantics. This branch implements the history of the incremental development of the model as presented in the paper, using the [alphaProlog](http://github.com/aprolog-lang) language. PBT is implemented in alphaCheck instead of QuickCheck.

[Testing Noninterference, Quickly (2013)](http://www.crash-safe.org/node/24/)

[Testing Noninterference, Quickly (2015)](https://arxiv.org/abs/1409.0393)


## Implementation LOG

Here are summarized how bugs were found and how features were added incrementally. Code is executed on a machine equipped with an Intel® Core™ i5-4200U CPU with a clock speed of 1.60GHz and 8GB of RAM. The operative system is a 64 bit Ubuntu 17.10 Artful, with Linux kernel version 4.13.0.

The property that the abstract machine must satisfy is *end-to-end noninterference*, which is represented in alphaCheck as

```
#check "end-to-end noninterference" 25:
    S = initial_state(P),
    S' = initial_state(P'),
    indistinguishable_states(S, S'),
    H = run(S),
    H' = run(S')
    =>
    indistinguishable_states(H, H').
```

The first implementation used the alphaProlog integer type to represent the program counter and labeled values stored in memory. However, when checking with alphaCheck not all variables are grounded, thus mathematical operations, such as addition, cause an error. Natural numbers were represented with [Peano numbers](https://en.wikipedia.org/wiki/Peano_axioms) instead, with a zero value and a successor function: ```o```, ```s(o)```, ```s(s(o))```, etc...

When a natural number appears in the samples outlined below, it is replaced with arabic numerals to enhance readability (for example ```s(s(s(o)))``` will be replaced with ```3```).

### Initial implementation

In [Section 2.4](https://arxiv.org/pdf/1409.0393.pdf#page=7), the authors list a first draft of the inference rules for the *step* relation. Within a minute of check execution time, the ```end-to-end noninterference``` property is falsified.

```
end-to-end noninterference
Checking depth 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
Total: 49.784716 s:
P = [push(private, N), push(private, o), store, halt]
P' = [push(private, N'), push(private, 1), store, halt]
S = (0, [], [(public, 0), (public, 0)], [push(private, N), push(private, 0), store, halt])
S' = (0, [], [(public, 0), (public, 0)], [push(private, N'), push(private, 1), store, halt])
H = (3, [], [(private, N), (public, 0)],[push(private, N), push(private, 0), store, halt])
H' = (3, [], [(public, 0), (private, N')], [push(private, N'), push(private, 1), store, halt])
```

This counterexample different to the one found by QuickCheck and reported by the authors in [Figure 1](https://arxiv.org/pdf/1409.0393.pdf#page=9): the first push writes a ```private``` value instead of a ```public``` one. It is more closely related to the counterexample described in [Figure 2](https://arxiv.org/pdf/1409.0393.pdf#page=9).


### Fixing (partially) the step rule for ```store```

The found counterexample illustrates a subtle point: the definition of noninterference allows the observer to distinguish between final memory states that differ only in their labels. However, to better study alphaCheck capability in finding bugs, only the first patch suggested by the authors is applied: it requires the ```store``` instruction to taint the stored value with the label on the pointer.

```
-step((PC, [(_, I), V | S], M, P)) = (s(PC), S, M', P) :- read(PC, P) = store, M' = save(M, I, V).
+step((PC, [(L, I), (L', N) | S], M, P)) = (s(PC), S, M', P) :- read(PC, P) = store, M' = save(M, I, (join(L, L'), N)).
```

Again the ```end-to-end noninterference``` property is falsified in about 100 seconds:

```
end-to-end noninterference
Checking depth 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21
Total: 97.654239 s:
P = [push(L', N), push(private, 0), store, halt]
P' = [push(L', N), push(private, 1), store, halt]
S = (0, [], [(public, 0), (public, 0)], [push(L', N), push(private, 0), store, halt])
S' = (0, [], [(public, 0), (public, 0)], [push(L', N), push(private, 1), store, halt])
H = (3, [], [(private, N), (public, 0)], [push(L', N), push(private, 0), store, halt])
H' = (3, [], [(public, 0), (private, N)], [push(L', N), push(private, 1), store, halt])
```

This time, the found counterexample is a generalisation of the one described in [Figure 2](https://arxiv.org/pdf/1409.0393.pdf#page=9).


### Fixing the step rule

As suggested by the authors, the ```step``` rule for ```store``` instructions must check whether the written memory address was private or public.

```
-step((PC, [(L, I), (L', N) | S], M, P)) = (s(PC), S, M', P) :- read(PC, P) = store, M' = save(M, I, (join(L, L'), N)).
+step((PC, [(L, I), (L', N) | S], M, P)) = (s(PC), S, M', P) :- read(PC, P) = store, (L'', _) = fetch(M, I), subsecret(L, L''), M' = save(M, I, (join(L, L'), N)).
```

The ```subsecret``` predicate means that the first label is equally secret or less secret than the second.

Running again the ```end-to-end noninterference``` property check, after several minutes, no counterexample is found. However a relevant bug is still present, and the reason why the check cannot find a small counterexample must be investigated.

After introducing a generator that builds a pair of indistinguishable programs with more structured instructions and limited values, a counterexample is found in about 10 minutes.

```
end-to-end noninterference
Checking depth 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38
Total: 537.391595 s:
H = (5, [], [(public, 0)], [push(private, 0), push(private, 0), add, push(public, 0), store, halt])
H' = (5, [], [(public, 1)], [push(private, 1), push(private, 0), add, push(public, 0), store, halt])
M = [(public, 0)]
P = [push(private, 0), push(private, 0), add, push(public, 0), store, halt]
P' = [push(private, 1), push(private, 0), add, push(public, 0), store, halt]
```

This counterexample has the same structure of the one described in [Figure 3](https://arxiv.org/pdf/1409.0393.pdf#page=9).


### Fixing the step rule one more time

The problem is that the taints on the arguments to ```add``` are not propagated to its result. The Store is needed in order to make the difference observable. The easy (and standard) fix is to use the join of the argument labels as the label of the result.

```
-step((PC, [(_, N), (_, N') | S], M, P)) = (s(PC), [(public, sum(N, N')) | S], M, P) :- read(PC, P) = add.
+step((PC, [(L, N), (L', N') | S], M, P)) = (s(PC), [(join(L, L'), sum(N, N')) | S], M, P) :- read(PC, P) = add.
```

Checking the ```end-to-end noninterference``` property with the custom generator introduced for the previous bug does not find any counterexample in a short time. A new generator has been designed, which generates programs upto a given length and that satisfies the following properties.

A *value* is a sequence of instructions that leave a value on the stack, which is:

  - a [**push** *V*] instruction, where the *V* value can be either 0 or 1 and have a public or private label;
  - a [*value*]+[**load**] sequence;
  - a [*value*]+[*value*]+[**add**] sequence.

A *program* is a sequence of instructions such that is either:

  - [**halt**], representing a program that terminates immediately;
  - ([*value*]+[*value*]+[**store**])~[*program*], where the (~) symbol indicates that the sequence ending with a **store** instruction is inserted into the *program* sequence, even at the beginning, but not at the end after the **halt** instruction.

Other instructions, such as **pop** and **noop** are not added because do not contribute to the modification of the memory state.

The generator yields couples of indistinguishable programs. The generator soundness has been validated upto program length 8 (which does not include the last **halt** instruction).

The check still cannot find a counterexample, but the following query does.

```
func violates_noninterference (number) = (program, program).
violates_noninterference(L) = (P, P') :-
    (P, P') = gen_programs(L),
    gen_memory(M),
    H = run((o, [], M, P)),
    H' = run((o, [], M, P')),
    not(indistinguishable_states(H, H')).

? (P, P') = violates_noninterference(7).
```

It generates programs of maximum length 7 (plus the final **halt** instruction) and within a minute it yields a counterexample with a behavior similar to the one reported in [Figure 4](https://arxiv.org/pdf/1409.0393.pdf#page=10): instead of producing two different public values it stores to the first address a 0 either public or private.

```
Query: (P,P') = violates_noninterference(7)
Yes.
P = [push(private, 0), push(public, 0), store, push(private, 0), load, push(public, 0), store, halt]
P' = [push(private, 0), push(public, 0), store, push(private, 1), load, push(public, 0), store, halt]

real	0m44,236s
```


### Fixing last bug

The solution to the previous bug is changing the ```load``` rule, such that when loading a low value through a high pointer the loaded value is tainted.

```
-step((PC, [(_, I) | S], M, P)) = (s(PC), [V | S], M, P) :- read(PC, P) = load, V = fetch(M, I).
+step((PC, [(L, I) | S], M, P)) = (s(PC), [(join(L, L'), N) | S], M, P) :- read(PC, P) = load, (L', N) = fetch(M, I).
```

No new counterexample is found up to depth 7.

```
Query: (P,P') = violates_noninterference(7)
No.

real	5m19,407s
```


### First comparison

Here are listed the results and execution times of checks run for each known bug present in the first version of the abstract machine, including the ones introduced in [Section 2.5](https://arxiv.org/pdf/1409.0393.pdf#page=10). Different generation strategies are compared:

  - **Check(25)**: using naive exhaustive generation upto depth 25
  - **Check(25)/Gen(7)**: using the same approach, but programs are derived from a generator which yields pairs of indistinguishable programs with at most 7 instructions (plus a terminal **halt** instruction)
  - **Query/Gen(L)**: a query navigates all programs yielded by the generator upto length *L*, until a counterexample is found

Each cell represents whether the check passed (no counterexample found) or failed (a counterexample was found). The number between parenthesis represents the depth where the counterexample was found.

| Bug      | Check(25)        | Check(25)/Gen(7) | Query/Gen(7) | Query/Gen(8) | Query/Gen(9) | Query/Gen(10)  |
|----------|------------------|------------------|--------------|--------------|--------------|----------------|
| none     | Pass 25'27''     | Pass 8'20''      | Pass 5'27''  | Pass 41'15'' | Pass 5h40'   | ongoing...     |
| STORE*AB | Fail(20) 53s     | Fail(20) 9.6s    | Fail 64ms    | Fail 63ms    | Fail 90ms    | Fail 100ms     |
| STORE*B  | Fail(21) 1'43''  | Fail(21) 31s     | Fail 63ms    | Fail 64ms    | Fail 102ms   | Fail 96ms      |
| ADD*     | Pass 23'57''     | Pass 8'08''      | Fail 48s     | Fail 43s     | Fail 6'53''  | Fail 42'40''   |
| LOAD*    | Pass 23'39''     | Pass 8'12''      | Fail 43s     | Fail 4'15''  | Fail 45'50'' | Fail 59s       |
| PUSH*    | Pass 21'49''     | Fail(22) 1'01''  | Fail 75ms    | Fail 54ms    | Fail 115ms   | Fail 118ms     |
| STORE*C  | Pass 24'10''     | Fail(19) 4.6s    | Fail 66ms    | Fail 55ms    | Fail 87ms    | Fail 110ms     |
| STORE*A  | Pass 24'34''     | Pass 8'09''      | Pass 5'21''  | Pass 39'54'' | Fail 9'35''  | Fail 1h01'     |


### Introducing jumps

The second version of the abstract machine includes a **jump** instruction, which sets the PC with the value on top of the stack. The ```end-to-end noninterference``` property does not find any counterexample upto depth 25 (nearly one hour of execution time).

The program generator has to be adapted to the new definition of the abstract machine. A new generator has been designed to include **jump** instructions and to manage numbers size directly. After about 25 minutes the query finds a counterexample similar to the one represented in [Figure 11](https://arxiv.org/pdf/1409.0393.pdf#page=15).

### Labeled PC

The solution proposed by the authors of the benchmark is to label the PC too. With the new definition, machine states are indistinguishable when either both PC are labeled private, or when bot PC are labeled public and stores and programs are indistinguishable.

After about 15 seconds of execution, the query finds a counterexample different from the one reported in [Figure 12](https://arxiv.org/pdf/1409.0393.pdf#page=16): instead of terminating with both public PCs, in the counterexample below the machines terminate with different PC labels.

```
Query: (P,P') = violates_noninterference(10)
Yes.
P = [push(private, 2), jump, push(public, 4), jump, halt]
P' = [push(private, 4), jump, push(public, 4), jump, halt]

real	0m15,820s
```

However in [Section 5.2](https://arxiv.org/pdf/1409.0393.pdf#page=17) the authors weaken the *end-to-end noninterference* property by considering only ending states that are both halting and with a PC labeled public. With this new weakened property, the alphaProlog query is not capable of finding a counterexample within several hours of execution.

Found two bugs in the generator code:

  - ```gen_value``` vas producing ```push``` instructions with public label and ```s(N)``` as value instead of ```o```;
  - ```gen_programs``` for public ```push``` instructions was calling itself recursively but lowering by one the size of values.

Even after these fixes, no counterexample is found within a couple of days of check execution. However it is known that this model version has still an error.
