func x2 (number) = number.
x2(o) = o.
x2(s(N)) = s(s(x2(N))).

func run_limited (number, state) = state.
run_limited(_, ((L, PC), S, M, P)) = ((L, PC), S, M, P) :- read(PC, P) = halt.
run_limited(s(N), X) = run_limited(N, step(X)).

pred is_public_pc (state).
is_public_pc(((public, _), _, _, _)).

func violates_noninterference (number) = (program, program).
violates_noninterference(L) = (P, P') :-
    M = gen_memory(L),
    (P, P') = gen_programs_upto(L),
    H = run_limited(x2(L), ((public, o), [], M, P)),
    is_public_pc(H),
    H' = run_limited(x2(L), ((public, o), [], M, P')),
    is_public_pc(H'),
    not(indistinguishable_states(H, H')).

? (P, P') = violates_noninterference(s(s(s(s(s(s(s(s(s(s(o))))))))))).
