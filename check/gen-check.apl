pred public_pc (state).
public_pc(((public, _), _, _, _)).

#check "end-to-end noninterference" 25:
    L = s(s(s(s(s(s(s(o))))))),
    M = gen_memory(L),
    (P, P') = gen_programs_upto(L),
    H = run(((public, o), [], M, P)),
    public_pc(H),
    H' = run(((public, o), [], M, P')),
    public_pc(H')
    =>
    indistinguishable_states(H, H').
