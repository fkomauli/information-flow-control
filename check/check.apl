pred public_pc (state).
public_pc(((public, _), _, _, _)).

#check "end-to-end noninterference" 25:
    S = initial_state(P),
    S' = initial_state(P'),
    indistinguishable_states(S, S'),
    H = run(S),
    public_pc(H),
    H' = run(S'),
    public_pc(H')
    =>
    indistinguishable_states(H, H').
