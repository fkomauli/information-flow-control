#check "end-to-end noninterference" 25:
    (P, P') = gen_programs(s(s(s(s(s(s(s(o)))))))),
    gen_memory(M),
    H = run((o, [], M, P)),
    H' = run((o, [], M, P'))
    =>
    indistinguishable_states(H, H').
