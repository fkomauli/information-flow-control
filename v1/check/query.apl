func violates_noninterference (number) = (program, program).
violates_noninterference(L) = (P, P') :-
    (P, P') = gen_programs(L),
    gen_memory(M),
    H = run((o, [], M, P)),
    H' = run((o, [], M, P')),
    not(indistinguishable_states(H, H')).

? (P, P') = violates_noninterference(s(s(s(s(s(s(s(o)))))))).
