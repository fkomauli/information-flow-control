#check "end-to-end noninterference" 25:
    S = initial_state(P),
    S' = initial_state(P'),
    indistinguishable_states(S, S'),
    H = run(S),
    H' = run(S')
    =>
    indistinguishable_states(H, H').
