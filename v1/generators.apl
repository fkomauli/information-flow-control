pred split (number, number, number).
split(o, o, o).
split(s(N), o, s(N)).
split(s(N), s(N'), N'') :- split(N, N', N'').

pred gen_values (number, program, program, program, program).
gen_values(s(o), P, P',[push(private, o) | P], [push(private, o) | P']).
gen_values(s(o), P, P',[push(private, o) | P], [push(private, s(o)) | P']).
gen_values(s(o), P, P',[push(private, s(o)) | P], [push(private, o) | P']).
gen_values(s(o), P, P',[push(private, s(o)) | P], [push(private, s(o)) | P']).
gen_values(s(o), P, P',[push(public, o) | P], [push(public, o) | P']).
gen_values(s(o), P, P',[push(public, s(o)) | P], [push(public, s(o)) | P']).
gen_values(s(s(L)), P, P', Q, Q') :- gen_values(s(L), [load | P], [load | P'], Q, Q').
gen_values(s(s(s(L))), P, P', R, R') :-
    split(s(s(L)), s(V), s(V')),
    gen_values(s(V), [add | P], [add | P'], Q, Q'),
    gen_values(s(V'), Q, Q', R, R').

pred gen_sequences (number, program, program, number, program, program).
gen_sequences(L, P, P', L, P, P').
gen_sequences(s(s(s(L))), P, P', L''', S, S') :-
    split(s(s(s(L))), s(s(s(L'))), L''),
    split(s(s(L')), s(V), s(V')),
    gen_values(s(V), [store | P], [store | P'], Q, Q'),
    gen_values(s(V'), Q, Q', R, R'),
    gen_sequences(L'', R, R', L''', S, S').
gen_sequences(s(s(s(L))), P, P', L''', S, S') :-
    split(s(s(s(L))), s(s(s(L'))), L''),
    split(s(s(L')), s(V), s(V')),
    gen_values(s(V), [store | P], [store | P'], Q, Q'),
    gen_sequences(L'', Q, Q', L''', R, R'),
    gen_values(s(V'), R, R', S, S').
gen_sequences(s(s(s(L))), P, P', L''', S, S') :-
    split(s(s(s(L))), s(s(s(L'))), L''),
    split(s(s(L')), s(V), s(V')),
    gen_sequences(L'', [store | P], [store | P'], L''', Q, Q'),
    gen_values(s(V), Q, Q', R, R'),
    gen_values(s(V'), R, R', S, S').

func gen_programs (number) = (program, program).
gen_programs(L) = (P, P') :- gen_sequences(L, [halt], [halt], _, P, P').

pred gen_memory (memory).
gen_memory([(public, o), (public, o)]).

(*
func gen_programs_unsound(number) = (program, program).
gen_programs_unsound(L) = (P, P') :- (P, P') = gen_programs(L), not(indistinguishable_programs(P, P')).

? (P, P') = gen_programs_unsound(s(s(s(s(s(s(s(s(o))))))))).
*)
