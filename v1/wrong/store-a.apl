%% NATURAL NUMBERS %%

number: type.
o: number.
s: number -> number.

func sum (number, number) = number.
sum(o, N) = N.
sum(s(N), N') = sum(N, s(N')).


%% LABELS %%

label: type.
public: label.
private: label.

pred subsecret (label, label).
subsecret(private, private).
subsecret(public, _).

func join (label, label) = label.
join(public, L) = L.
join(private, _) = private.


%% MACHINE STATES %%

type value = (label, number).

instruction: type.
push: value -> instruction.
pop: instruction.
load: instruction.
store: instruction.
add: instruction.
noop: instruction.
halt: instruction.

type pc = number.
type stack = [value].
type memory = [value].
type program = [instruction].

type state = (pc, stack, memory, program).

func fetch (memory, number) = value.
fetch([V | _], o) = V.
fetch([_ | M], s(I)) = fetch(M, I).

func save (memory, number, value) = memory.
save([_ | M], o, V) = [V | M].
save([V' | M], s(I), V) = [V' | save(M, I, V)].

func read (pc, program) = instruction.
read(o, [I | _]) = I.
read(s(PC), [_ | P]) = read(PC, P).


%% INDISTINGUISHABILITY %%

pred indistinguishable_values (value, value).
indistinguishable_values((private, _), (private, _)).
indistinguishable_values((public, V), (public, V)).

pred indistinguishable_instructions (instruction, instruction).
indistinguishable_instructions(I, I).
indistinguishable_instructions(push(V), push(V')) :- indistinguishable_values(V, V').

pred indistinguishable_memories ([value], [value]).
indistinguishable_memories([], []).
indistinguishable_memories([V | M], [V' | M']) :- indistinguishable_values(V, V'), indistinguishable_memories(M, M').

pred indistinguishable_programs (program, program).
indistinguishable_programs([], []).
indistinguishable_programs([I | P], [I' | P']) :- indistinguishable_instructions(I, I'), indistinguishable_programs(P, P').

pred indistinguishable_states (state, state).
indistinguishable_states((_, _, M, P), (_, _, M', P')) :- indistinguishable_memories(M, M'), indistinguishable_programs(P, P').


%% DYNAMIC SEMANTICS %%

func initial_state (program) = state.
initial_state(P) = (o, [], [], P).
initial_state(P) = (o, [], [(public, o) | M], P) :- (o, [], M, P) = initial_state(P).

func step (state) = state.
step((PC, S, M, P)) = (s(PC), S, M, P) :- read(PC, P) = noop.
step((PC, S, M, P)) = (s(PC), [V | S], M, P) :- read(PC, P) = push(V).
step((PC, [_ | S], M, P)) = (s(PC), S, M, P) :- read(PC, P) = pop.
step((PC, [(L, I) | S], M, P)) = (s(PC), [(join(L, L'), N) | S], M, P) :- read(PC, P) = load, (L', N) = fetch(M, I).
step((PC, [(L, I), (L', N) | S], M, P)) = (s(PC), S, M', P) :- read(PC, P) = store, (L'', _) = fetch(M, I), subsecret(L, L''), M' = save(M, I, (L', N)).
step((PC, [(L, N), (L', N') | S], M, P)) = (s(PC), [(join(L, L'), sum(N, N')) | S], M, P) :- read(PC, P) = add.

func steps (state) = state.
steps(S) = S.
steps(S) = steps(step(S)).

func run (state) = state.
run(X) = (PC, S, M, P) :- (PC, S, M, P) = steps(X), read(PC, P) = halt.
